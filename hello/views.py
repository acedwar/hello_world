from subprocess import Popen, PIPE, STDOUT

from django.shortcuts import render
from django.http import HttpResponse

import requests

from .models import Greeting


def listing(request, chapter, listing):
    context = {}
    context['chapter'] = chapter = int(chapter)
    context['listing'] = listing = int(listing)

    filepath = 'listings/Listing_{}-{}.py'.format(chapter, listing)

    with open(filepath) as f:
        context['title'] = ''
        i = 0
        for line in f:
            if line.startswith('# '):
                i += 1
                if i == 5:
                    context['title'] = line[2:]
                    break

    p = Popen(['python', filepath], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    input_ = request.GET.get('input')
    context['output'], unused = p.communicate(input_)

    return render(request, 'listing.html', context)


def index(request):
    r = requests.get('http://httpbin.org/status/418')
    print r.text
    return HttpResponse('<pre>' + r.text + '</pre>')


def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})

